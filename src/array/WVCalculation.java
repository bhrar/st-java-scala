package array;

public class WVCalculation {

	private final int[] wvArray ;
	public WVCalculation(int[] input) {
		wvArray = input;
	}
	
	public int run() {
		int sum = 0;
		//1. find the maximum
		int maxId = 0;
		for (int i=1; i<wvArray.length; ++i) {
			if (wvArray[i] > wvArray[maxId]) {
				maxId = i;
			}
		}
		//2. scan the inputs of either sides
		//2.1 from left to the maximum
		int peak = wvArray[0];
		for (int i=0; i<maxId; ++i) {
			if (wvArray[i] > peak) {
				peak = wvArray[i];
			}
			sum += peak - wvArray[i] ;
		}
		//2.2 from right to the maximum
		peak = wvArray[wvArray.length - 1];
		for (int i=wvArray.length-1; i>maxId; --i) {
			if (wvArray[i] > peak) {
				peak = wvArray[i];
			}
			sum += peak - wvArray[i];
		}
		return sum;
	}
	

	public static void main(String[] args) {
		int[] inputs = {1,2,3,4,6,23,5,23,2,4,4,5};
		WVCalculation wv = new WVCalculation(inputs);
		System.out.println(wv.run());
	}

}
