package string;

public class LPS {

	public LPS() {
		// TODO Auto-generated constructor stub
	}

	public String run(String input) {
		
		int maxLength = 0;
		String maxPS = "";
		
		//1. scan the entire input string to find LPS
		for (int i=0; i<input.length(); ++i) {
			//1.1 early stop 
			if (i + maxLength/2 > input.length()) {
				break;
			}
			//1.2 start the current char and go to both sides
			int curPos = 1;
			while (i - curPos >= 0 && i + curPos < input.length() 
					&& input.charAt(i-curPos) == input.charAt(i+curPos)) {
				if (curPos * 2 + 1 > maxLength) {
					maxLength = curPos * 2 + 1;
					maxPS = input.substring(i-curPos, i+curPos+1);
				}
				curPos++;
			}
			
			//1.3 when the length is even
			curPos = 0;
			while (i-curPos >= 0 && i+curPos+1 < input.length()
					&& input.charAt(i-curPos) == input.charAt(i+curPos+1)) {
				if (curPos * 2 + 2 > maxLength) {
					maxLength = curPos * 2 + 2;
					maxPS = input.substring(i-curPos, i+curPos+2);
				}
				curPos++;
			}
		}
		return maxPS;
	}
	
	public static void main(String[] args) {
		String input = "qwerrewqasdfwrasrqweasdfaerq3412rasfasdsaffasdfasdfasdfaopuqpwerajs df;ajerasdfasdf a;sdjf;akjsdf;qwerarewq";
		LPS lps = new LPS();
		System.out.println(String.format("The LPS of %s is %s", input, lps.run(input)));
	}

}
